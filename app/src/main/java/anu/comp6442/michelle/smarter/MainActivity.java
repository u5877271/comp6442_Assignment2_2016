package anu.comp6442.michelle.smarter;
/**
 * (1)Class MainActivity Created by Lanmixue Mao(u5877271) on 13/04/16, including Init(), Onclick(),
 * checkValidity(),print(), checkBack(), noteCheck(), noteShow().
 * checkBack() approach by Leonid taken from
 * http://codereview.stackexchange.com/questions/84497/android-calculator-app
 * (2)Class Calculate Created by Tianyu Li(u5953423) on 14/04/2016, including cal(), FP(), showError().
 * cal() approach by Mike Dalisay taken from
 * https://www.codeofaninja.com/2014/08/android-calculator-tutorial-source-code-example.html
 */

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.util.StringTokenizer;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{

    /* Declare widget objects */
    public TextView screen,memo,note;
    public Button[] btn = new Button[10];
    public Button plus,minus,multiply,divide,equal,sin,cos,tan,
            log,ln,sqrt,square,left,right,back,dot,exit,clear;
    /* Declare calculate number*/

    public String num_first;
    public String num_second;
    public boolean screenControl= true;// Control screen, true: restart screen,false: keep on screen
    public boolean noteLock = true;// Lock notes screen, true: keep on screen,false: lock screen
    public boolean equalFlag = true;//true: screen before press "=" button ,false: screen after press "=" button

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Init();
    }

    public void Init(){
        /* Instantiate widgets*/
        screen = (TextView) findViewById(R.id.resultText);
        memo = (TextView) findViewById(R.id.memo);
        note = (TextView) findViewById(R.id.note);
        btn[0] = (Button) findViewById(R.id.button0);
        btn[1] = (Button) findViewById(R.id.button1);
        btn[2] = (Button) findViewById(R.id.button2);
        btn[3] = (Button) findViewById(R.id.button3);
        btn[4] = (Button) findViewById(R.id.button4);
        btn[5] = (Button) findViewById(R.id.button5);
        btn[6] = (Button) findViewById(R.id.button6);
        btn[7] = (Button) findViewById(R.id.button7);
        btn[8] = (Button) findViewById(R.id.button8);
        btn[9] = (Button) findViewById(R.id.button9);
        dot = (Button) findViewById(R.id.dot);
        plus = (Button) findViewById(R.id.plus);
        minus = (Button) findViewById(R.id.minus);
        multiply = (Button) findViewById(R.id.multiply);
        divide = (Button) findViewById(R.id.divide);
        equal = (Button) findViewById(R.id.equal);
        left = (Button) findViewById(R.id.left);
        right = (Button) findViewById(R.id.right);
        sin = (Button) findViewById(R.id.sin);
        cos = (Button) findViewById(R.id.cos);
        tan = (Button) findViewById(R.id.tan);
        log = (Button) findViewById(R.id.log);
        ln = (Button) findViewById(R.id.ln);
        sqrt = (Button) findViewById(R.id.sqrt);
        square = (Button) findViewById(R.id.square);
        exit = (Button) findViewById(R.id.exit);
        back = (Button) findViewById(R.id.back);
        clear = (Button) findViewById(R.id.clear);
        /* Click listener for launching button */
        // set onclick listener for number key
        for (int i = 0; i < 10; i++) {
            btn[i].setOnClickListener(this);
        }
        // set onclick listener for +-x÷ etc
        plus.setOnClickListener(this);
        minus.setOnClickListener(this);
        multiply.setOnClickListener(this);
        divide.setOnClickListener(this);
        equal.setOnClickListener(this);
        sin.setOnClickListener(this);
        cos.setOnClickListener(this);
        tan.setOnClickListener(this);
        log.setOnClickListener(this);
        ln.setOnClickListener(this);
        sqrt.setOnClickListener(this);
        square.setOnClickListener(this);
        left.setOnClickListener(this);
        right.setOnClickListener(this);
        dot.setOnClickListener(this);
        exit.setOnClickListener(this);
        back.setOnClickListener(this);
        clear.setOnClickListener(this);
    }
    /* Capture keyboard commands */
    String[] notes = new String[100];
    int noteNum = 0; // notes Command pointer
    public void onClick(View v) {
        // get command from keyboard
        String command = ((Button) v).getText().toString();
        // get result from screen
        String result = screen.getText().toString();
        // check screen type validity
        if (equalFlag == false
                && "0123456789.()sincostanlnlog+-×÷√^".indexOf(command) != -1) {
            // check validity of string on the screen
            if (checkValidity(result)) {
                if ("+-×÷√^)".indexOf(command) != -1) {
                    for (int i = 0; i < result.length(); i++) {
                        notes[noteNum] = String.valueOf(result.charAt(i));
                        noteNum++;
                    }
                    screenControl = false;
                }
            }else {
                screen.setText("0");
                screenControl = true;
                noteNum = 0;
                noteLock = true;
                note.setText("Welcome to Smarter！");
            }
            equalFlag = true;
        }
        if (noteNum > 0)
            noteCheck(notes[noteNum - 1], command);
        else if (noteNum == 0) {
            noteCheck("#", command);
        }
        if ("0123456789.()sincostanlnlog+-×÷√^".indexOf(command) != -1
                && noteLock) {
            notes[noteNum] = command;
            noteNum++;
        }
        // If correctly input, the typing msg will show on the screen
        if ("0123456789.()sincostanlnlog+-×÷√^".indexOf(command) != -1
                && noteLock) {
            print(command);
            // If input the back space before press "="
        } else if (command.compareTo("←") == 0 && equalFlag) {
            // Remove 3 chars at once
            if (checkBack(result) == 3) {
                if (result.length() > 3)
                    screen.setText(result.substring(0, result.length() - 3));
                else if (result.length() == 3) {
                    screen.setText("0");
                    screenControl = true;
                    noteNum = 0;
                    note.setText("Welcome to Smarter！");
                }
                // Remove 2 chars one by one
            } else if (checkBack(result) == 2) {
                if (result.length() > 2)
                    screen.setText(result.substring(0, result.length() - 2));
                else if (result.length() == 2) {
                    screen.setText("0");
                    screenControl = true;
                    noteNum = 0;
                    note.setText("Welcome to Smarter！");
                }
                // Remove 1 char
            } else if (checkBack(result) == 1) {
                // If inputed chars comply with validity, we can remove one char
                if (checkValidity(result)) {
                    if (result.length() > 1)
                        screen.setText(result.substring(0, result.length() - 1));
                    else if (result.length() == 1) {
                        screen.setText("0");
                        screenControl = true;
                        noteNum = 0;
                        note.setText("Welcome to Smarter！");
                    }
                    // If inputed chars violate validity, we need to remove all chars
                } else {
                    screen.setText("0");
                    screenControl = true;
                    noteNum = 0;
                    note.setText("Welcome to Smarter！");
                }
            }
            if (screen.getText().toString().compareTo("-") == 0
                    || equalFlag == false) {
                screen.setText("0");
                screenControl = true;
                noteNum = 0;
                note.setText("Welcome to Smarter！");
            }
            noteLock = true;
            if (noteNum > 0)
                noteNum--;
            // If input the back space after press "="
        } else if (command.compareTo("←") == 0 && equalFlag == false) {
            // Clear all the msg on the screen and only display "0"
            screen.setText("0");
            screenControl = true;
            noteNum = 0;
            noteLock = true;
            note.setText("Welcome to Smarter！");
            // If input the clear
        } else if (command.compareTo("c") == 0) {
            // set the Memo to show "0"
            memo.setText("0");
            // set the screen to show "0"
            screen.setText("0");
            // set the screen control equals true
            screenControl = true;
            // clear buffer cache to 0
            noteNum = 0;
            // shows you can continue to input
            noteLock = true;
            // shows before you input "="
            equalFlag = true;
            note.setText("Welcome to Smarter！");
            // If input the exit, the app will be exited
        } else if (command.compareTo("exit") == 0) {
            System.exit(0);
            // If input char equals to "=" and complies with validity
        } else if (command.compareTo("=") == 0 && noteLock && checkValidity(result)
                && equalFlag) {
            noteNum = 0;
            // shows you can not continue to input
            noteLock = false;
            // shows after you input "="
            equalFlag = false;
            // save original expression
            num_first = result;
            // replace the operators for convenience calculation
            result = result.replaceAll("sin", "s");
            result = result.replaceAll("cos", "c");
            result = result.replaceAll("tan", "t");
            result = result.replaceAll("log", "g");
            result = result.replaceAll("ln", "l");
            // set the screen control equals true
            screenControl = true;
            // replace -1x to -
            num_second = result.replaceAll("-", "-1×");
            // calculate the result
            new Calculate().cal(num_second);
        }
        // shows you can continue to input
        noteLock = true;
    }
    /*
     * check result validity，the return value can be true or false
     * If the result only contains 0123456789.()sincostanlnlog+-×÷√^，return true
     * Except the above values，return false
     */
    public boolean checkValidity(String result) {
            int i = 0;
            for (i = 0; i < result.length(); i++) {
                if (result.charAt(i) != '0' && result.charAt(i) != '1'
                        && result.charAt(i) != '2' && result.charAt(i) != '3'
                        && result.charAt(i) != '4' && result.charAt(i) != '5'
                        && result.charAt(i) != '6' && result.charAt(i) != '7'
                        && result.charAt(i) != '8' && result.charAt(i) != '9'
                        && result.charAt(i) != '.' && result.charAt(i) != '-'
                        && result.charAt(i) != '+' && result.charAt(i) != '×'
                        && result.charAt(i) != '÷' && result.charAt(i) != '√'
                        && result.charAt(i) != '^' && result.charAt(i) != 's'
                        && result.charAt(i) != 'n' && result.charAt(i) != 'c'
                        && result.charAt(i) != 'o' && result.charAt(i) != 't'
                        && result.charAt(i) != 'a' && result.charAt(i) != 'l'
                        && result.charAt(i) != 'g' && result.charAt(i) != 'i'
                        && result.charAt(i) != '(' && result.charAt(i) != ')'
                        )
                    break;
            }
            if (i == result.length()) {
                return true;
            } else {
                return false;
            }
        }
    /**
     * Shows the msg on the screen
     */
    public void print(String str) {
        // output after clearing the screen
        if (screenControl) {
            screen.setText(str);
        } else {
            screen.append(str);
        }
        screenControl = false;
    }
    /*
    * check function，return value can be 3,2,1 (means should remove how many numbers: Three+Two+One) Shows how back space remove
    * return 3, means end of str can be one of sin,cos,tan,log, should remove 3 chars at once
    * return 2, means end of str can be ln,should remove 2 chars at once
    * return 1, means except return 3 or 2, other return value only need to remove 1 char
    */
    public int checkBack(String str) {
        if ((str.charAt(str.length() - 1) == 'n'
                && str.charAt(str.length() - 2) == 'i' && str.charAt(str
                .length() - 3) == 's')
                || (str.charAt(str.length() - 1) == 's'
                && str.charAt(str.length() - 2) == 'o' && str
                .charAt(str.length() - 3) == 'c')
                || (str.charAt(str.length() - 1) == 'n'
                && str.charAt(str.length() - 2) == 'a' && str
                .charAt(str.length() - 3) == 't')
                || (str.charAt(str.length() - 1) == 'g'
                && str.charAt(str.length() - 2) == 'o' && str
                .charAt(str.length() - 3) == 'l')) {
            return 3;
        } else if ((str.charAt(str.length() - 1) == 'n' && str.charAt(str
                .length() - 2) == 'l')) {
            return 2;
        } else {
            return 1;
        }
    }
    /*
     * check notes，check former and later result.
     * The front and back of decimal point can be omitted,using zero(0) instead.
     * The first digit of number can be zero(0).
     */
    public void noteCheck(String noteFir, String notesec) {

        // NoteError:give error hint，noteDetail: explain different expressions
        int noteError = 0, noteDetail = 0;
        // noteType shows the type of commands
        int noteType1 = 0, noteType2 = 0;
        // record the number of brackets
        int bracket = 0;
        // “+-x÷√^” can not put in first digit
        if (noteFir.compareTo("#") == 0
                && (notesec.compareTo("÷") == 0
                || notesec.compareTo("×") == 0
                || notesec.compareTo("+") == 0
                || notesec.compareTo(")") == 0
                || notesec.compareTo("√") == 0
                || notesec.compareTo("^") == 0)) {
            noteError = -1;
        }
        // Define the type of last index of screen string
        else if (noteFir.compareTo("#") != 0) {
            if (noteFir.compareTo("(") == 0) {
                noteType1 = 1;
            } else if (noteFir.compareTo(")") == 0) {
                noteType1 = 2;
            } else if (noteFir.compareTo(".") == 0) {
                noteType1 = 3;
            } else if ("0123456789".indexOf(noteFir) != -1) {
                noteType1 = 4;
            } else if ("+-×÷".indexOf(noteFir) != -1) {
                noteType1 = 5;
            } else if ("√^".indexOf(noteFir) != -1) {
                noteType1 = 6;
            } else if ("sincostanlogln".indexOf(noteFir) != -1) {
                noteType1 = 7;
            }
            // Define the screen button type
            if (notesec.compareTo("(") == 0) {
                noteType2 = 1;
            } else if (notesec.compareTo(")") == 0) {
                noteType2 = 2;
            } else if (notesec.compareTo(".") == 0) {
                noteType2 = 3;
            } else if ("0123456789".indexOf(notesec) != -1) {
                noteType2 = 4;
            } else if ("+-×÷".indexOf(notesec) != -1) {
                noteType2 = 5;
            } else if ("√^".indexOf(notesec) != -1) {
                noteType2 = 6;
            } else if ("sincostanlogln".indexOf(notesec) != -1) {
                noteType2 = 7;
            }

            switch (noteType1) {
                case 1:
                    // left bracket split joint right bracket, +x÷（negative sign “-” exclude）or √^
                    if (noteType2 == 2
                            || (noteType2 == 5 && notesec.compareTo("-") != 0)
                            || noteType2 == 6)
                        noteError = 1;
                    break;
                case 2:
                    // right bracket split joint left bracket，number and +-x÷sin^
                    if (noteType2 == 1 || noteType2 == 3 || noteType2 == 4
                            || noteType2 == 7)
                        noteError = 2;
                    break;
                case 3:
                    // “.” split joint left bracket or sincos
                    if (noteType2 == 1 || noteType2 == 7)
                        noteError = 3;
                    // two consecutive screens “.”
                    if (noteType2 == 3)
                        noteError = 8;
                    break;
                case 4:
                    // numbers split joint left bracket or sincos
                    if (noteType2 == 1 || noteType2 == 7)
                        noteError = 4;
                    break;
                case 5:
                    // +-x÷ split joint right bracket or +-x÷√^
                    if (noteType2 == 2 || noteType2 == 5 || noteType2 == 6)
                        noteError = 5;
                    break;
                case 6:
                    // √^ split joint right bracket，+-x÷√^ and sincos
                    if (noteType2 == 2 || noteType2 == 5 || noteType2 == 6
                            || noteType2 == 7)
                        noteError = 6;
                    break;
                case 7:
                    // sincos split joint right bracket, +-x÷√^ and sincos
                    if (noteType2 == 2 || noteType2 == 5 || noteType2 == 6
                            || noteType2 == 7)
                        noteError = 7;
                    break;
            }
        }
        // check repeatability of decimal point
        if (noteError == 0 && notesec.compareTo(".") == 0) {
            int decimalPoint = 0;
            for (int i = 0; i < noteNum; i++) {
                // If screen one decimal point, the counter of decimal point adds 1
                if (notes[i].compareTo(".") == 0) {
                    decimalPoint++;
                }
                // If screen any operator,reset the counter of decimal point
                if (notes[i].compareTo("sin") == 0
                        || notes[i].compareTo("cos") == 0
                        || notes[i].compareTo("tan") == 0
                        || notes[i].compareTo("log") == 0
                        || notes[i].compareTo("ln") == 0
                        || notes[i].compareTo("√") == 0
                        || notes[i].compareTo("^") == 0
                        || notes[i].compareTo("÷") == 0
                        || notes[i].compareTo("×") == 0
                        || notes[i].compareTo("-") == 0
                        || notes[i].compareTo("+") == 0
                        || notes[i].compareTo("(") == 0
                        || notes[i].compareTo(")") == 0) {
                    decimalPoint = 0;
                }
            }
            decimalPoint++;
            // If the counter of decimal point is greater than 1, it shows that decimal point is repeated.
            if (decimalPoint > 1) {
                noteError = 8;
            }
        }
        // check right bracket if it is matched
        if (noteError == 0 && notesec.compareTo(")") == 0) {
            int rightBracket = 0;
            for (int i = 0; i < noteNum; i++) {
                // If has a left bracket, the counter of right bracket adds 1
                if (notes[i].compareTo("(") == 0) {
                    rightBracket++;
                }
                // If has a right bracket, the counter of right bracket minuses 1
                if (notes[i].compareTo(")") == 0) {
                    rightBracket--;
                }
            }
            // If the counter of right bracket equals 0, it shows that none of left bracket matches the right bracket.
            if (rightBracket == 0) {
                noteError = 10;
            }
        }
        // check validity of screented "="
        if (noteError == 0 && notesec.compareTo("=") == 0) {
            // bracket matching
            int noteBracket = 0;
            for (int i = 0; i < noteNum; i++) {
                if (notes[i].compareTo("(") == 0) {
                    noteBracket++;
                }
                if (notes[i].compareTo(")") == 0) {
                    noteBracket--;
                }
            }
            // If noteBracket is greater than 0, it shows that the left bracket is non-matched.
            if (noteBracket > 0) {
                noteError = 9;
                bracket = noteBracket;
            } else if (noteBracket == 0) {
                // If the former operator is as follows, it shows that the "=" is illegal
                if ("√^sincostanlogln".indexOf(noteFir) != -1) {
                    noteError = 6;
                }
                // If the former operator is as follows, it shows that the "=" is illegal
                if ("+-×÷".indexOf(noteFir) != -1) {
                    noteError = 5;
                }
            }
        }
        // If the commands are as follows,displaying the relate information
        if (notesec.compareTo("c") == 0)
            noteDetail = 1;
        if (notesec.compareTo("←") == 0)
            noteDetail = 2;
        if (notesec.compareTo("sin") == 0)
            noteDetail = 3;
        if (notesec.compareTo("cos") == 0)
            noteDetail = 4;
        if (notesec.compareTo("tan") == 0)
            noteDetail = 5;
        if (notesec.compareTo("log") == 0)
            noteDetail = 6;
        if (notesec.compareTo("ln") == 0)
            noteDetail = 7;
        if (notesec.compareTo("√") == 0)
            noteDetail = 8;
        if (notesec.compareTo("^") == 0)
            noteDetail = 9;
        // Show the error and help information
        noteShow(bracket, noteError, noteDetail, noteFir, notesec);

    }

    /* Note information feedback */
    public void noteShow(int bracket, int notecode1, int notecode2,
                         String notecommand1, String notecommand2) {

        String noteMsg = "";
        if (notecode1 != 0)
            noteLock = false;// screen incorrect
        switch (notecode1) {
            case -1:
                noteMsg = notecommand2 + "  cannot use as the first operator\n";
                break;
            case 1:
                noteMsg = notecommand1 + "  should screen:number/(/./-/function at behind\n";
                break;
            case 2:
                noteMsg = notecommand1 + "  should screen：)/operator at behind\n";
                break;
            case 3:
                noteMsg = notecommand1 + "  should screen:)/number/operator at behind\n";
                break;
            case 4:
                noteMsg = notecommand1 + "  should screen:)/./number /operator at behind\n";
                break;
            case 5:
                noteMsg = notecommand1 + "  should screen:(/./number/function at behind\n";
                break;
            case 6:
                noteMsg = notecommand1 + "  should screen:(/./number at behind\n";
                break;
            case 7:
                noteMsg = notecommand1 + "  should screen:(/./number at behind\n";
                break;
            case 8:
                noteMsg = "Repetitive decimal point\n";
                break;
            case 9:
                noteMsg = "Cannot calculate，lack of " + bracket + " )";
                break;
            case 10:
                noteMsg = "no need  )";
                break;
        }
        switch (notecode2) {
            case 1:
                noteMsg = noteMsg + "[C Direction: reset]";
                break;
            case 2:
                noteMsg = noteMsg + "[← Direction: backspace]";
                break;
            case 3:
                noteMsg = noteMsg + "[sin] an example of sin function:\n"
                        + "sin30 = 0.5 \n"
                        + "notes:If used with other functions, should adds bracket:\n"
                        + "sin(cos45)";
                break;
            case 4:
                noteMsg = noteMsg + "[cos] an example of cos function:\n"
                        + "cos60 = 0.5 \n"
                        + "notes:If used with other functions, should adds bracket:\n"
                        + "cos(sin45)";
                break;
            case 5:
                noteMsg = noteMsg + "[tan] an example of tan function：\n"
                        + "tan45 = 1 \n"
                        + "notes：If used with other functions, should adds bracket：\n"
                        + "tan(cos45)";
                break;
            case 6:
                noteMsg = noteMsg + "[log] an example of log function:\n"
                        + "log10 = log(5+5) = 1\n"
                        + "notes:If used with other functions, should adds bracket:\n"
                        + "log(tan45)";
                break;
            case 7:
                noteMsg = noteMsg + "[ln] an example of ln function:\n"
                        + "ln10 = le(5+5) = 2.3   lne = 1\n"
                        + "notes:If used with other functions, should adds bracket:\n"
                        + "ln(tan45)";
                break;
            case 8:
                noteMsg = noteMsg + "[√] usage examples:used for taking the ROOT of a number\n"
                        + "E.g.: 27√3 = 3\n";
                break;
            case 9:
                noteMsg = noteMsg + "[^] usage examples:used for taking the SQUARE of a number\n"
                        + "E.g.: 2^3 = 8\n";
                break;
        }
        // present notes to Note Display Frame
        note.setText(noteMsg);

    }
    /**
     * Created by Tianyu Li on 14/04/2016.
     */
    public class Calculate {

        public static final int LENGTH = 100;
        public double pi = 4 * Math.atan(1);// π

        /* Constructor */
        public Calculate() {
            super();
        }

        /*
         * Computation Expression: scan from left to right
         * number input into number stack;operator input into operator stack
         * priority level 1: + -
         * priority level 2: × ÷
         * priority level 3: log ln sin cos tan
         * priority level 4: √ ^
         * the operational characters inside brackets have 4 levels priority higher than same operational characters outside brackets
         * if current operational character has higher priority than the stack top, push it to the stack top,
         * if current operational character has lower priority than the stack top, pop out one operational character from the stack and calculate with the two numbers
         * repeat the process until current operational character have higher priority than the one at the stack top
         * after scanning, calculate the left operational characters and numbers successively
         */
        public void cal(String result) {
            int weightCurr = 0, weightTemp = 0,CountOpe = 0, CountNum = 0, flag = 1;
            // weightCurr is the standard priority of the content in the same pair of brackets，record the changes of priority levels temporatively using weightTemp
            // CountOpe is counter for weight[] and operator[]；CountNum is counter for number[].
            // flag is the signal to separate positive and negative numbers. 1 represents positive, -1 represents negative.
            int priority[]; // save the priority levels of operational characters in the operator stack
            double number[]; // save the numbers using number[], count them using topNum
            char ch, ch_i, operator[];// Use operator[] to save the operational characters, count them using topOp
            String num;// record numbers, str is divided by +-×÷()sctgl!√^，so the strings between +-×÷()sctgl!√^ are numbers
            priority = new int[LENGTH];
            number = new double[LENGTH];
            operator = new char[LENGTH];
            String expression = result;
            StringTokenizer expToken = new StringTokenizer(expression,
                    "+-×÷()sctgl√^");
            int i = 0;
            while (i < expression.length()) {
                ch = expression.charAt(i);
                // Analyzing the positive and negative values
                if (i == 0) {
                    if (ch == '-')
                        flag = -1;
                } else if (expression.charAt(i - 1) == '(' && ch == '-')
                    flag = -1;
                // Get number and transfer the plus or minus sign to the number
                if (ch <= '9' && ch >= '0' || ch == '.' || ch == 'E') {
                    num = expToken.nextToken();
                    ch_i = ch;
                    Log.e("result", ch + "--->" + i);
                    // fetch the whole number
                    while (i < expression.length()
                            && (ch_i <= '9' && ch_i >= '0' || ch_i == '.' || ch_i == 'E')) {
                        ch_i = expression.charAt(i++);
                        Log.e("result", "the value of i is：" + i);
                    }
                    // Move the pointer back to the previous position
                    if (i >= expression.length())
                        i -= 1;
                    else {
                        i -= 2;
                    }
                    if (num.compareTo(".") == 0)
                        number[CountNum++] = 0;
                        // transfer the plus or minus sign to the number
                    else {
                        number[CountNum++] = Double.parseDouble(num) * flag;
                        flag = 1;
                    }
                }
                // Calculating the priority of operators
                if (ch == '(')
                    weightCurr += 4;
                if (ch == ')')
                    weightCurr -= 4;
                if (ch == '-' && flag == 1 || ch == '+' || ch == '×'
                        || ch == '÷' || ch == 's' || ch == 'c' || ch == 't'
                        || ch == 'g' || ch == 'l' || ch == '√' || ch == '^') {
                    switch (ch) {
                        // "+" and "-" have the lowest priority level 1
                        case '+':
                        case '-':
                            weightTemp = 1 + weightCurr;
                            break;
                        // "x" and "÷" have level 2 priority
                        case '×':
                        case '÷':
                            weightTemp = 2 + weightCurr;
                            break;
                        // "sin","cos", "log", "ln", "!"have level 3 priority
                        case 's':
                        case 'c':
                        case 't':
                        case 'g':
                        case 'l':
                            weightTemp = 3 + weightCurr;
                            break;
                        // Other operations have level 4 priority, which is the highest
                        // case '^':
                        // case '√':
                        default:
                            weightTemp = 4 + weightCurr;
                            break;
                    }
                    // If the current priority level is higher than the top element in stack. then directly push it to stack
                    if (CountOpe == 0 || priority[CountOpe - 1] < weightTemp) {
                        priority[CountOpe] = weightTemp;
                        operator[CountOpe] = ch;
                        CountOpe++;
                        // Otherwise take operators in stack one by one, until the top operator in stack has lower priority compared to current operator
                    } else {
                        while (CountOpe > 0 && priority[CountOpe - 1] >= weightTemp) {
                            switch (operator[CountOpe - 1]) {
                                // Take corresponding elements in the array of numbers to calculate
                                case '+':
                                    number[CountNum - 2] += number[CountNum - 1];
                                    break;
                                case '-':
                                    number[CountNum - 2] -= number[CountNum - 1];
                                    break;
                                case '×':
                                    number[CountNum - 2] *= number[CountNum - 1];
                                    break;
                                // Analyzing whether the divisor is 0 or not
                                case '÷':
                                    if (number[CountNum - 1] == 0) {
                                        showError(1, num_first);
                                        return;
                                    }
                                    number[CountNum - 2] /= number[CountNum - 1];
                                    break;
                                case '√':
                                    if (number[CountNum - 1] == 0
                                            || (number[CountNum - 2] < 0 && number[CountNum - 1] % 2 == 0)) {
                                        showError(2, num_first);
                                        return;
                                    }
                                    number[CountNum - 2] = Math.pow(
                                            number[CountNum - 2], 1 / number[CountNum - 1]);
                                    break;
                                case '^':
                                    number[CountNum - 2] = Math.pow(
                                            number[CountNum - 2], number[CountNum - 1]);
                                    break;
                                // When doing the calculation, converse between angle and radian.
                                // sine
                                case 's':
                                    number[CountNum - 1] = Math
                                            .sin((number[CountNum - 1] / 180)
                                                    * pi);
                                    CountNum++;
                                    break;
                                // cosine
                                case 'c':
                                    number[CountNum - 1] = Math
                                            .cos((number[CountNum - 1] / 180)
                                                    * pi);
                                    CountNum++;
                                    break;
                                // tangent
                                case 't':
                                    if ((Math.abs(number[CountNum - 1]) / 90) % 2 == 1) {
                                        showError(2, num_first);
                                        return;
                                    }
                                    number[CountNum - 1] = Math
                                            .tan((number[CountNum - 1] / 180)
                                                    * pi);
                                    CountNum++;
                                    break;
                                // logarithm, log
                                case 'g':
                                    if (number[CountNum - 1] <= 0) {
                                        showError(2, num_first);
                                        return;
                                    }
                                    number[CountNum - 1] = Math
                                            .log10(number[CountNum - 1]);
                                    CountNum++;
                                    break;
                                // Natural logarithm, ln
                                case 'l':
                                    if (number[CountNum - 1] <= 0) {
                                        showError(2, num_first);
                                        return;
                                    }
                                    number[CountNum - 1] = Math
                                            .log(number[CountNum - 1]);
                                    CountNum++;
                                    break;
                            }
                            // Take the next element in stack to analyze
                            CountNum--;
                            CountOpe--;
                        }
                        // Push the operators to stack
                        priority[CountOpe] = weightTemp;
                        operator[CountOpe] = ch;
                        CountOpe++;
                    }
                }
                i++;
            }
            // Successively get stack operators to calculate
            while (CountOpe > 0) {
                // for addition, subtraction and multiplication, directly take the last 2 elements in the array for calculation
                switch (operator[CountOpe - 1]) {
                    case '+':
                        number[CountNum - 2] += number[CountNum - 1];
                        break;
                    case '-':
                        number[CountNum - 2] -= number[CountNum - 1];
                        break;
                    case '×':
                        number[CountNum - 2] *= number[CountNum - 1];
                        break;
                    // Situations like in divisions the divisor can not be zero
                    case '÷':
                        if (number[CountNum - 1] == 0) {
                            showError(1, num_first);
                            return;
                        }
                        number[CountNum - 2] /= number[CountNum - 1];
                        break;
                    case '√':
                        if (number[CountNum - 1] == 0
                                || (number[CountNum - 2] < 0 && number[CountNum - 1] % 2 == 0)) {
                            showError(2, num_first);
                            return;
                        }
                        number[CountNum - 2] = Math.pow(
                                number[CountNum - 2], 1 / number[CountNum - 1]);
                        break;
                    case '^':
                        number[CountNum - 2] = Math.pow(number[CountNum - 2],
                                number[CountNum - 1]);
                        break;
                    // sine
                    case 's':
                        number[CountNum - 1] = Math
                                .sin((number[CountNum - 1] / 180) * pi);

                        CountNum++;
                        break;
                    // cosine
                    case 'c':
                        number[CountNum - 1] = Math
                                .cos((number[CountNum - 1] / 180) * pi);
                        CountNum++;
                        break;
                    // tangent
                    case 't':
                        if ((Math.abs(number[CountNum - 1]) / 90) % 2 == 1) {
                            showError(2, num_first);
                            return;
                        }
                        number[CountNum - 1] = Math
                                .tan((number[CountNum - 1] / 180) * pi);
                        CountNum++;
                        break;
                    // Logarithm log
                    case 'g':
                        if (number[CountNum - 1] <= 0) {
                            showError(2, num_first);
                            return;
                        }
                        number[CountNum - 1] = Math.log10(number[CountNum - 1]);
                        CountNum++;
                        break;
                    // Natural logarithm ln
                    case 'l':
                        if (number[CountNum - 1] <= 0) {
                            showError(2, num_first);
                            return;
                        }
                        number[CountNum - 1] = Math.log(number[CountNum - 1]);
                        CountNum++;
                        break;
                }
                // Get next element in the stack to calculate
                CountNum--;
                CountOpe--;
            }
            // If the number is too large, display error message
            if (number[0] > 7.3E306) {
                showError(3, num_first);
                return;
            }
            // Final result output
            screen.setText(String.valueOf(FP(number[0])));
            note.setText("calculation complete, press button C to continue");
            memo.setText(num_first + "=" + String.valueOf(FP(number[0])));
        }

        /*
         * FP = floating point. Control the length of decimal digits to reach accuracy, otherwise FP will be involved
         * The situations similar to "0.6-0.2=0.39999999999999997"， can be resolved by FP. In this case the result will be 0.4. This format have precision of 15 digits after the decimal point.
         */
        public double FP(double n) {
            // NumberFormat format=NumberFormat.getInstance(); //Creating a formatting class f
            // format.setMaximumFractionDigits(18); // Setting format for decimal digits
            DecimalFormat format = new DecimalFormat("0.#############");
            return Double.parseDouble(format.format(n));
        }
        /* Error message showing: after pressing button "=", if an error appears in calculation formula, display an error message*/
        public void showError(int code, String str) {
            String message = "";
            switch (code) {
                case 1:
                    message = "Zero can not be divisor";
                    break;
                case 2:
                    message = "Wrong function format";
                    break;
                case 3:
                    message = "the value is too large";
            }
            screen.setText("\"" + str + "\"" + ": " + message);
            note.setText(message + "\n" + "calculation complete, press button C to continue");
        }



    }


}
