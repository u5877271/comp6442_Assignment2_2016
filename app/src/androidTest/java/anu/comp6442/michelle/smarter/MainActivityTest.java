package anu.comp6442.michelle.smarter;

import android.app.Application;
import android.app.Instrumentation;
import android.test.ApplicationTestCase;
import android.test.ActivityInstrumentationTestCase2;
import android.widget.Button;
import android.widget.HorizontalScrollView;
import android.widget.TextView;
import junit.*;
import junit.framework.Assert;
import junit.framework.TestCase;

//import org.junit.Test;

import dalvik.annotation.TestTarget;

/**
 * <a href="http://d.android.com/tools/testing/testing_android.html">Testing Fundamentals</a>
 */
public class MainActivityTest extends ActivityInstrumentationTestCase2<MainActivity> {
    private Instrumentation instrumentation;
    private MainActivity mainActivity;
    private MainActivity mainActivitytestNote;

    public MainActivityTest() {
        super(MainActivity.class);
    }

    @Override
    protected void setUp() throws Exception{
        super.setUp();
        instrumentation = getInstrumentation();
        mainActivity = (MainActivity)getActivity();
        mainActivitytestNote = (MainActivity)getActivity();
    }

    //@Test
    public void testActivity() throws Exception {
        Button btn0 = (Button)mainActivity.findViewById(R.id.button0);
        Button btn1 = (Button)mainActivity.findViewById(R.id.button1);
        Button btn2 = (Button)mainActivity.findViewById(R.id.button2);
        Button btn3 = (Button)mainActivity.findViewById(R.id.button3);
        Button btn4 = (Button)mainActivity.findViewById(R.id.button4);
        Button btn5 = (Button)mainActivity.findViewById(R.id.button5);
        Button btn6 = (Button)mainActivity.findViewById(R.id.button6);
        Button btn7 = (Button)mainActivity.findViewById(R.id.button7);
        Button btn8 = (Button)mainActivity.findViewById(R.id.button8);
        Button btn9 = (Button)mainActivity.findViewById(R.id.button9);
        Button btnplus = (Button)mainActivity.findViewById(R.id.plus);
        Button btnminus = (Button)mainActivity.findViewById(R.id.minus);
        Button btnmultiply = (Button)mainActivity.findViewById(R.id.multiply);
        Button btndivide = (Button)mainActivity.findViewById(R.id.divide);
        Button btnequal = (Button)mainActivity.findViewById(R.id.equal);
        Button btnlog = (Button)mainActivity.findViewById(R.id.log);
        Button btnln = (Button)mainActivity.findViewById(R.id.ln);
        Button btnsquare = (Button)mainActivity.findViewById(R.id.square);
        Button btnsqrt = (Button)mainActivity.findViewById(R.id.sqrt);
        Button btntan = (Button)mainActivity.findViewById(R.id.tan);
        Button btnsin = (Button)mainActivity.findViewById(R.id.sin);
        Button btncos = (Button)mainActivity.findViewById(R.id.cos);
        Button btnback = (Button)mainActivity.findViewById(R.id.back);
        Button btnclear = (Button)mainActivity.findViewById(R.id.clear);
        Button btndot = (Button)mainActivity.findViewById(R.id.dot);
        Button btnleft = (Button)mainActivity.findViewById(R.id.left);
        Button btnright = (Button)mainActivity.findViewById(R.id.right);
        HorizontalScrollView result = (HorizontalScrollView)mainActivity.findViewById(R.id.scrollview);

        //case "36/9+1.875*24-1=48"
        btn3.performClick();
        btn6.performClick();
        btndivide.performClick();
        btn9.performClick();
        btnplus.performClick();
        btn1.performClick();
        btndot.performClick();
        btn8.performClick();
        btn7.performClick();
        btn5.performClick();
        btnmultiply.performClick();
        btn2.performClick();
        btn4.performClick();
        btnminus.performClick();
        btn1.performClick();
        btnequal.performClick();
        getInstrumentation().waitForIdleSync();
        Assert.assertEquals(result.toString(),"48");

        Assert.assertFalse(mainActivity.checkValidity("1234fgg")); //Should be false
        Assert.assertEquals(mainActivity.checkBack("sin"), 3); //sin should be 3
        Assert.assertEquals(mainActivity.checkBack("ln"),2); //ln should be 2
        Assert.assertEquals(mainActivity.checkBack("3+2"), 1); //"2" in "3+2" should be 1
        //main.noteCheck()
        //Assert.assertNotNull

        //case "sin0+cos0+tan0=1"
        btnclear.performClick();
        btnback.performClick();
        btnsin.performClick();
        btn0.performClick();
        btnplus.performClick();
        btncos.performClick();
        btn0.performClick();
        btnplus.performClick();
        btntan.performClick();
        btn0.performClick();
        getInstrumentation().waitForIdleSync();
        Assert.assertEquals(result.toString(), "1");

        //case "log(ln1+(sqrt10)^2)=1"
        btnclear.performClick();
        btnlog.performClick();
        btnleft.performClick();
        btnln.performClick();
        btn1.performClick();
        btnplus.performClick();
        btnleft.performClick();
        btnsqrt.performClick();
        btn1.performClick();
        btn0.performClick();
        btnright.performClick();
        btnsquare.performClick();
        btn2.performClick();
        btnright.performClick();
        getInstrumentation().waitForIdleSync();
        Assert.assertEquals(result.toString(), "1");



    }

}