package anu.comp6442.michelle.smarter;

import android.test.InstrumentationTestCase;

import junit.framework.Assert;

/**
 * Created by Tianyu Li(u5953423) on 16/05/16.
 */
public class ExampleTest extends InstrumentationTestCase{
    public void test() throws Exception {
        final int expected = 1;
        final int reality = 1;
        Assert.assertEquals(expected, reality);
    }
}
