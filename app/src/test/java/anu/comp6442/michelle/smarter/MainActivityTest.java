package anu.comp6442.michelle.smarter;

import android.app.Instrumentation;
import android.test.ActivityInstrumentationTestCase2;
import android.widget.Button;
import android.widget.HorizontalScrollView;

import junit.framework.Assert;

import android.app.Application;
import android.app.Instrumentation;
import android.test.ApplicationTestCase;
import android.test.ActivityInstrumentationTestCase2;
import android.widget.Button;
import android.widget.HorizontalScrollView;
import android.widget.TextView;
import junit.*;
import junit.framework.Assert;
import junit.framework.TestCase;

import dalvik.annotation.TestTarget;

/**
 * <a href="http://d.android.com/tools/testing/testing_android.html">Testing Fundamentals</a>
 */
public class MainActivityTest extends ActivityInstrumentationTestCase2<MainActivity> {
    private Instrumentation instrumentation;
    private MainActivity mainActivity;

    public MainActivityTest() {
        super(MainActivity.class);
    }

    @Override
    protected void setUp() throws Exception{
        super.setUp();
        instrumentation = getInstrumentation();
        mainActivity = (MainActivity)getActivity();
    }


    public void testActivity() throws Exception {
        Button btn0 = (Button)mainActivity.findViewById(R.id.button0);
        Button btn1 = (Button)mainActivity.findViewById(R.id.button1);
        Button btnplus = (Button)mainActivity.findViewById(R.id.plus);
        HorizontalScrollView result = (HorizontalScrollView)mainActivity.findViewById(R.id.scrollview);
        Assert.assertFalse(mainActivity.checkValidity("1234fgg")); //Should be false
        Assert.assertEquals(mainActivity.checkBack("sin"), 3); //sin should be 3
        Assert.assertEquals(mainActivity.checkBack("ln"),2); //sin should be 3
        Assert.assertEquals(mainActivity.checkBack("3+2"), 1); //sin should be 3
        //main.noteCheck()
        btn0.performClick();
        btnplus.performClick();
        btn1.performClick();
        getInstrumentation().waitForIdleSync();
        Assert.assertEquals(result.toString(),"1");
        //Assert.assertEquals(main.identity(4), 4);  //FF
        //Assert.assertEquals(main.identity(10), 10);   //FT


    }

}